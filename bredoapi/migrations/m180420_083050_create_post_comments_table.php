<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_comments`.
 */
class m180420_083050_create_post_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_comments', [
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'post_id' => $this->integer(),
            'content' => $this->text()
        ]);

        $this->createIndex(
            'idx-post_comments-user_id',
            'post_comments',
            'user_id'
        );

        $this->addForeignKey(
            'fk-post_comments-user_id',
            'post_comments',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-post_comments-post_id',
            'post_comments',
            'post_id'
        );

        $this->addForeignKey(
            'fk-post_comments-post_id',
            'post_comments',
            'post_id',
            'posts',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-post_comments-user_id',
            'post_comments'
        );
        $this->dropIndex(
            'idx-post_comments-user_id',
            'post_comments'
        );
        $this->dropForeignKey(
            'fk-post_comments-post_id',
            'post_comments'
        );
        $this->dropIndex(
            'idx-post_comments-post_id',
            'post_comments'
        );
        $this->dropTable('post_comments');
    }
}

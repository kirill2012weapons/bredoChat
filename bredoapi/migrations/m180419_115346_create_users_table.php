<?php

use yii\db\Migration;
/**
 * Handles the creation of table `users`.
 */
class m180419_115346_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'id' => $this->primaryKey(),
            'email' => $this->string(20)->unique(),
            'hash_pass' => $this->text(),
            'nick' => $this->string(10),
            'user_img' => $this->string('50'),
            'descriptions_user' => $this->string(20)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}

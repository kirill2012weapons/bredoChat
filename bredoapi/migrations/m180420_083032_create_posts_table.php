<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m180420_083032_create_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'content' => $this->text()
        ]);

        $this->createIndex(
            'idx-posts-user_id',
            'posts',
            'user_id'
        );

        $this->addForeignKey(
            'fk-posts-user_id',
            'posts',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-posts-user_id',
            'posts'
        );
        $this->dropIndex(
            'idx-posts-user_id',
            'posts'
        );
        $this->dropTable('posts');
    }
}

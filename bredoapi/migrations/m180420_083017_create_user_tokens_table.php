<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_tokens`.
 */
class m180420_083017_create_user_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_tokens', [
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'access_token' => $this->text()
        ]);

        $this->createIndex(
          'idx-user_tokens-user_id',
          'user_tokens',
          'user_id'
        );

        $this->addForeignKey(
            'fk-user_tokens-user_id',
            'user_tokens',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_tokens-user_id',
            'user_tokens'
        );
        $this->dropIndex(
            'idx-user_tokens-user_id',
            'user_tokens'
        );
        $this->dropTable('user_tokens');
    }
}

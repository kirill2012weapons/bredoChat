<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 25.04.2018
 * Time: 10:29
 */

namespace app\commands;

use app\controllers\SocketServer;
use yii\console\Controller;

class ServerController extends Controller{

    public function actionStart($port = null) {

        $server = new SocketServer();
        if ($port) {
            $server->port = $port;
        }
        $server->start();

    }

}
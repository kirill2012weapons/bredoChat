<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 20.04.2018
 * Time: 12:33
 */

namespace app\models;

use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Post extends ActiveRecord {

    const SCENARIO_GUEST_POST = 'guest_post';
    const SCENARIO_USER_POST = 'user_post';

    public static function tableName() {

        return '{{posts}}';

    }

    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_GUEST_POST] = ['content'];
        $scenarios[self::SCENARIO_USER_POST] = ['user_id', 'content'];
        return $scenarios;

    }

    public function rules() {

        return [
            [ ['user_id', 'content'], 'required', 'on' => self::SCENARIO_USER_POST ],
            [ ['content'], 'required', 'on' => self::SCENARIO_GUEST_POST ],
            [ ['user_id'], 'integer'  ],
            [ ['user_id'], 'default', 'value' => 'NULL', 'on' =>self::SCENARIO_GUEST_POST ]
        ];

    }

    public function behaviors() {

        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function($event) {

                    return new Expression('NOW()');

                }
            ],
        ];

    }

}
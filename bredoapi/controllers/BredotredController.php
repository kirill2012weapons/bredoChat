<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 20.04.2018
 * Time: 12:29
 */

namespace app\controllers;

use app\models\jsonCode;
use app\models\Post;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class BredotredController extends Controller {

    public function actionPost() {

        $newPost = new Post;
        $newPost->scenario = Post::SCENARIO_GUEST_POST;

        if (empty($newPost)) {
            return jsonCode::set('',['No content field']);
        };

        $newPost->content = \Yii::$app->request->getBodyParam('content');

        if ($newPost->validate()) {
            $newPost->save();
            return jsonCode::set('',$newPost->errors);
        } else {
            return jsonCode::set('',$newPost->errors);
        }

    }

    public function actionGetPosts() {

        $post = Post::find()
            ->limit(20)
            ->indexBy(
                function ($raw) {
                    return 'POST_ID_' . $raw['id'];
                })
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return jsonCode::set($post, '');

    }

    public function actionNoHttp() {

        return jsonCode::set('', ['404']);

    }

    public function beforeAction($action) {
//        return parent::beforeAction($action);
        if (\Yii::$app->request->isAjax) {
            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        } else {
            $this->asJson(jsonCode::set('',['No Ajax']));
            return false;
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ]

        ];

        return $behaviors;
    }

//    public function actions()
//    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//        ];
//    }

}
$( document ).ready(function() {

    window.posts = Posts();
    posts.get();

    window.binding = Bindings();

});

function Posts() {

    var doc = document;
    var inner_con = doc.querySelector('[chat-app]');

    var conn = null;

    var _boxUsers = doc.querySelector('[box-users]');

    var _ = {};

    _._openSocket = function () {
        conn = new WebSocket('ws://localhost:8080');
        conn.onmessage = function(e) {
            var _d = JSON.parse(e.data);
            if (_d.data.User) {
                console.dir(JSON.parse(e.data));
                _.renderUser(_d.data);
            } else if (_d.data.disconnect) {
                _.deleteUser(_d.data);
            } else {
                // console.dir(JSON.parse(e.data));
                _.render(_d.data, 'FUCK');
            }
        };
        conn.onopen = function(e) {
            // conn.send(JSON.stringify({'action':'SendMessage', 'mess' : 'Kirill1233123123'}));
        };
    };

    _.deleteUser = function (data) {
        var _user = _boxUsers.querySelector('[user-indentety="' + data.disconnect + '"]');
        _boxUsers.removeChild(_user);
    };

    _.renderUser = function (data) {

        var user = doc.createElement('div');
        user.classList.add('wrap-mess');
        user.setAttribute('user-indentety', data.User);
        user.setAttribute('user-online', '');
            var _p_name = doc.createElement('p');
            _p_name.innerHTML = data.User;
        user.appendChild(_p_name);

        _boxUsers.appendChild(user);

    };

    _.render = function (data) {

        if (arguments.length == 1) {
            for (var post in data) {

                var chatDiv = null;
                chatDiv = doc.createElement('div');
                chatDiv.setAttribute('chat-part', '');

                var chatDiv_h3 = doc.createElement('h3');
                chatDiv_h3.setAttribute('chat-name', '');
                if (data[post].user_id == null) {
                    chatDiv_h3.innerHTML = 'GUEST';
                } else {
                    chatDiv_h3.innerHTML = data[post].user_id;
                }

                var chatDiv_p = doc.createElement('p');
                chatDiv_p.setAttribute('chat-content', '');
                chatDiv_p.innerHTML = data[post].content;

                var chatDiv_span = doc.createElement('span');
                chatDiv_span.setAttribute('chat-date', '');
                chatDiv_span.innerHTML = data[post].created_at;

                chatDiv.appendChild(chatDiv_h3);
                chatDiv.appendChild(chatDiv_p);
                chatDiv.appendChild(chatDiv_span);

                var before_ell = doc.querySelector('[chat-app]');

                if (before_ell.children.length == 0) {
                    inner_con.insertBefore(chatDiv, null);
                } else inner_con.insertBefore(chatDiv, null);

            }
        } else {
            for (var post in data) {

                var chatDiv = null;
                chatDiv = doc.createElement('div');
                chatDiv.setAttribute('chat-part', '');

                var chatDiv_h3 = doc.createElement('h3');
                chatDiv_h3.setAttribute('chat-name', '');
                if (data[post].user_id == null) {
                    chatDiv_h3.innerHTML = 'GUEST';
                } else {
                    chatDiv_h3.innerHTML = data[post].user_id;
                }

                var chatDiv_p = doc.createElement('p');
                chatDiv_p.setAttribute('chat-content', '');
                chatDiv_p.innerHTML = data[post].content;

                var chatDiv_span = doc.createElement('span');
                chatDiv_span.setAttribute('chat-date', '');
                chatDiv_span.innerHTML = data[post].created_at;

                chatDiv.appendChild(chatDiv_h3);
                chatDiv.appendChild(chatDiv_p);
                chatDiv.appendChild(chatDiv_span);

                var before_ell = doc.querySelector('[chat-app]');

                if (before_ell.children.length == 0) {
                    inner_con.insertBefore(chatDiv, null);
                } else inner_con.insertBefore(chatDiv, before_ell.children[0]);

            }
        }

    };

    _.get = function () {
        $.ajax({
            method: "GET",
            url: "http://localhost/bred-chat/bredoapi/api/post/get",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (!data.success) return;
                console.dir(data);
                _.render(data.data);
                _._openSocket();
            },
            error: function (err1, err2) {
              console.dir(err1);
              console.dir(err2);
            }
        });
    };

    _.set = function (content) {
        var data = {};
        data.mess = content;
        data.action = 'SendMessage';
        conn.send(JSON.stringify({'action':'SendMessage', 'mess' : content}));
    };

    return _;
}

function Bindings() {
    var _ = {};
    var doc = document;
    var textArea = doc.querySelector('[chat-text-area]');
    var button_sub = doc.querySelector('[chat-button-sub]');

    if (!button_sub) {
        console.dir('no btn');
        return;
    }

    button_sub.addEventListener('click', function (event) {
        event.preventDefault();
        posts.set(textArea.value);
    });

    return _;
}